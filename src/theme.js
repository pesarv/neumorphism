const light = {
  "--accent": "#95d3ff",
  "--accent-light": "#bee0f9",
  "--bg": "#ecf0f3",
  "--bg-hover": "linear-gradient(145deg, rgb(238, 242, 245), rgb(236, 240, 243))",
  "--color": "#77c6ff",
  "--filter": "url(#filter-light)",
  "--shadow-dark": "#d1d9e6",
  "--shadow-light": "white",
}

const dark = {
  "--accent": "#fed300",
  "--accent-light": "#b69700",
  "--bg": "#353535",
  "--bg-hover": "#353535",
  "--color": "#fed300",
  "--filter": "url(#filter-dark)",
  "--shadow-dark": "#202020",
  "--shadow-light": "#454545",
}

export { light, dark };