import React from "react";

export default function ActionMenuIcon({ height = 18, width = 18, dur = "300ms" }) {
  return (
    <svg height={height} width={width} className="action-menu-icon">
      <line x1="0" y1="1" x2={width / 2} y2="1">
        <animate
          className="animate-open"
          begin="indefinite"
          attributeName="y2"
          dur={dur}
          to={height / 2}
          fill="freeze"
        />
        <animate
          className="animate-close"
          begin="indefinite"
          attributeName="y2"
          dur={dur}
          to="1"
          fill="freeze"
        />
      </line>
      <line x1={width / 2} y1="1" x2={width} y2="1">
        <animate
          className="animate-open"
          begin="indefinite"
          attributeName="y1"
          dur={dur}
          to={height / 2}
          fill="freeze"
        />
        <animate
          className="animate-close"
          begin="indefinite"
          attributeName="y1"
          dur={dur}
          to="1"
          fill="freeze"
        />
      </line>

      <line x1="0" y1={height / 2} x2={width / 2} y2={height / 2}>
        <animate
          className="animate-open"
          begin="indefinite"
          attributeName="x2"
          dur={dur}
          to="0"
          fill="freeze"
        />
        <animate
          className="animate-close"
          begin="indefinite"
          attributeName="x2"
          dur={dur}
          to={width / 2}
          fill="freeze"
        />
      </line>
      <line x1={width / 2} y1={height / 2} x2={width} y2={height / 2}>
        <animate
          className="animate-open"
          begin="indefinite"
          attributeName="x1"
          dur={dur}
          to={width}
          fill="freeze"
        />
        <animate
          className="animate-close"
          begin="indefinite"
          attributeName="x1"
          dur={dur}
          to={width / 2}
          fill="freeze"
        />
      </line>

      <line x1="0" y1={height - 1} x2="10" y2={height - 1}>
        <animate
          className="animate-open"
          begin="indefinite"
          attributeName="y2"
          dur={dur}
          to={height / 2}
          fill="freeze"
        />
        <animate
          className="animate-close"
          begin="indefinite"
          attributeName="y2"
          dur={dur}
          to={height - 1}
          fill="freeze"
        />
      </line>
      <line x1={width / 2} y1={height - 1} x2={width} y2={height - 1}>
        <animate
          className="animate-open"
          begin="indefinite"
          attributeName="y1"
          dur={dur}
          to={height / 2}
          fill="freeze"
        />
        <animate
          className="animate-close"
          begin="indefinite"
          attributeName="y1"
          dur={dur}
          to={height - 1}
          fill="freeze"
        />
      </line>
    </svg>
  );
}
