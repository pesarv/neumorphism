import React, { useState } from "react";
import "./action-menu.scss";
import classnames from "classnames";
import ActionMenuIcon from "./ActionMenuIcon";

export default function ActionMenu() {
  const items = [
    {
      icon: {
        name: "js",
        color: "#f0db4f",
      },
    },
    {
      icon: {
        name: "vuejs",
        color: "#41B883",
      },
    },
    {
      icon: {
        name: "react",
        color: "#61DBFB",
      },
    },
    {
      icon: {
        name: "angular",
        color: "#dd0031",
      },
    },
    {
      icon: {
        name: "node-js",
        color: "#68A063",
      },
    },
  ];

  const [isOpen, setIsOpen] = useState(false);

  const handleClick = (e) => {
    if (isOpen) {
      e.currentTarget
        .querySelectorAll(".animate-close")
        .forEach((el) => el.beginElement());
    } else {
      e.currentTarget
        .querySelectorAll(".animate-open")
        .forEach((el) => el.beginElement());
    }
    setIsOpen((prevState) => !prevState);
  };

  return (
    <div className={classnames("action-menu", { "action-menu--open": isOpen })}>
      <button className="action-menu-btn neu-btn" onClick={handleClick}>
        <ActionMenuIcon />
      </button>

      <div className="action-menu__items">
        {items.map((item, index) => {
          return (
            <div
              key={index}
              style={{ "--item-index": index }}
              className="action-menu__item"
            >
              <div className="action-menu__item-shadow">
                <button className="action-menu__item-btn neu-btn">
                  <i
                    className={`fab fa-${item.icon.name}`}
                    style={{ color: item.icon.color, fontSize: "1rem" }}
                  ></i>
                </button>
              </div>
            </div>
          );
        })}
      </div>

      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" className="goo-svg">
        <defs>
          <filter id="goo">
            <feGaussianBlur
              in="SourceGraphic"
              stdDeviation="12"
              result="blur"
            />
            <feColorMatrix
              in="blur"
              mode="matrix"
              values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7"
              result="goo"
            />
            <feBlend in="SourceGraphic" in2="goo" />
          </filter>
        </defs>
      </svg>
    </div>
  );
}
