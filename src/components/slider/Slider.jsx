import React, { useRef } from 'react';
import './slider.scss'

export const Slider = ({value, setValue}) => {

  const sliderRef = useRef();  

  const onMouseDown = () => {
    document.addEventListener("mousemove", mouseMove);
    document.addEventListener("mouseup", mouseUp);
  }

  const mouseMove = (e) => {
    const { width, x } = sliderRef.current.getBoundingClientRect();
    const mouseLeft = e.pageX - x;
    const precent = Math.round(mouseLeft / width * 100);
    setValue(precent < 0 ? 0 : (precent > 100 ? 100 : precent));
  }

  const mouseUp = () => {
    document.removeEventListener("mousemove", mouseMove);
    document.removeEventListener("mouseup", mouseUp);
  }

  return (
    <div className="slider" ref={sliderRef} style={{ "--slider-value": `${value}%` }}>
        <div className="slider-value"></div>
        <div className="slider-knob" onMouseDown={onMouseDown} onDragStart={(e) => { e.preventDefault() }}>
          <div className="slider-knob-inner">
          </div>
        </div>
      </div>
  )
}
