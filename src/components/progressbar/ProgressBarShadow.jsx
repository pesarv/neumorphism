import React from "react";

export default function ProgressBarShadow() {
  return (
    <>
      <g filter="var(--filter)">
        <path
          d="M29.2896 170.711C33.207 174.628 39.5144 174.628 43.4318 170.711L57.5739 156.569C61.4913 152.652 61.4913 146.344 57.5739 142.427C52.0024 136.855 47.5828 130.241 44.5676 122.961C41.5523 115.682 40.0003 107.88 40.0003 100C40.0003 92.1211 41.5523 84.3189 44.5676 77.0394C47.5828 69.7598 52.0024 63.1455 57.5739 57.574C63.1454 52.0024 69.7598 47.5829 77.0393 44.5676C84.3189 41.5523 92.121 40.0004 100 40.0004C107.88 40.0004 115.682 41.5523 122.961 44.5676C130.241 47.5829 136.855 52.0024 142.427 57.574C147.998 63.1455 152.418 69.7598 155.433 77.0394C158.448 84.3189 160 92.1211 160 100C160 107.88 158.448 115.682 155.433 122.961C152.418 130.241 147.998 136.855 142.427 142.427C138.509 146.344 138.509 152.652 142.427 156.569L156.569 170.711C160.486 174.628 166.794 174.628 170.711 170.711C184.696 156.726 194.221 138.907 198.079 119.509C201.938 100.111 199.957 80.0037 192.388 61.731C184.819 43.4585 172.002 27.8408 155.557 16.8528C139.112 5.86475 119.778 -4.99238e-05 100 3.18731e-10C80.2223 -4.90733e-05 60.8884 5.86475 44.4435 16.8528C27.9986 27.8408 15.1813 43.4585 7.61241 61.731C0.0434146 80.0037 -1.93705 100.111 1.92147 119.509C5.78 138.907 15.3042 156.726 29.2896 170.711V170.711Z"
          fill="var(--bg)"
        />
      </g>
      <defs>
        <filter
          id="filter-light"
          x="-4"
          y="-4"
          width="208.001"
          height="181.649"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="BackgroundImageFix"
            result="shape"
          />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dx="4" dy="4" />
          <feGaussianBlur stdDeviation="4" />
          <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0.819608 0 0 0 0 0.85098 0 0 0 0 0.901961 0 0 0 1 0"
          />
          <feBlend mode="normal" in2="shape" result="effect1_innerShadow" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dx="-4" dy="-4" />
          <feGaussianBlur stdDeviation="4" />
          <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 1 0"
          />
          <feBlend
            mode="normal"
            in2="effect1_innerShadow"
            result="effect2_innerShadow"
          />
        </filter>

        <filter
          id="filter-dark"
          x="-4"
          y="-4"
          width="208.001"
          height="181.649"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="BackgroundImageFix"
            result="shape"
          />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dx="4" dy="4" />
          <feGaussianBlur stdDeviation="4" />
          <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0.12549 0 0 0 0 0.12549 0 0 0 0 0.12549 0 0 0 1 0"
          />
          <feBlend mode="normal" in2="shape" result="effect1_innerShadow" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dx="-4" dy="-4" />
          <feGaussianBlur stdDeviation="4" />
          <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0.270833 0 0 0 0 0.270833 0 0 0 0 0.270833 0 0 0 1 0"
          />
          <feBlend
            mode="normal"
            in2="effect1_innerShadow"
            result="effect2_innerShadow"
          />
        </filter>
      </defs>
    </>
  );
}
