import React from "react";
import ProgressBarShadow from "./ProgressBarShadow";

export default function Progressbar({value}) {
  const circles = [
    { r: 70, cap: "round" },
    { r: 75, cap: "square" },
    { r: 80, cap: "square" },
    { r: 85, cap: "square" },
    { r: 90, cap: "round" },
  ];

  const getC = (r) => {
    return r * 2 * Math.PI;
  };

  const getOffSet = (r) => {
    return getC(r) - (getC(r) / 4) * 3 * value / 100
  }

  return (
    <svg
      width="200"
      height="200"
      viewBox="0 0 200 200"
      fill="none"
    >
      <ProgressBarShadow />

      {circles.map((c, index) => {
        return (
          <circle
            key={index}
            cx="100"
            cy="100"
            r={c.r}
            stroke="var(--accent)"
            strokeWidth="10"
            strokeLinecap={c.cap}
            strokeDasharray={getC(c.r)}
            strokeDashoffset={getOffSet(c.r)}
          ></circle>
        );
      })}
    </svg>
  );
}
