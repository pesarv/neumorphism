import "./App.scss";
import React, { useState } from "react";
import { Slider } from "./components/slider/Slider";
import Progressbar from "./components/progressbar/Progressbar";
import ActionMenu from "./components/action-menu/ActionMenu";
import { light, dark } from "./theme"

function App() {

  const [sliderValue, setSliderValue] = useState(0);
  const [theme, setTheme] = useState("light")
  
  const changeTheme = () => {
    const nextTheme = theme === "light" ? dark : light;
    for (const customProp in nextTheme) {
      document.documentElement.style.setProperty(
        customProp,
        nextTheme[customProp]
      );
    }
    setTheme(theme => theme === "light" ? "dark" : "light");
  }

  return (
    <div className="App">
      <ActionMenu />
      <Progressbar value={sliderValue}/>
      <Slider value={sliderValue} setValue={setSliderValue}/>
      <div className="btn-shadow">
        <button className="btn neu-btn" onClick={changeTheme}>{theme === "light" ? "Dark" : "Light"}</button>
      </div>
    </div>
  );
}

export default App;
